import java.util.ArrayList;
import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("asd");
        list.add("asdga");
        list.add("");
        list.add("d");
        list.add("d");
        list.add("d");
        list.add("d");
        list.add("sd");
        list.add("  ");
        System.out.println(list);
        System.out.println(list.stream()
                .filter(s -> s.length() > 0)
                .mapToInt(String::length)
                .summaryStatistics()
                .getCount());
    }

}
