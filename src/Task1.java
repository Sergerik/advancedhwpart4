import java.util.stream.IntStream;

public class Task1 {

    public static void main(String[] args) {

        System.out.println(IntStream
                .rangeClosed(1, 100)
                .filter(count -> count % 2 == 0)
                .reduce(0, Integer::sum));
    }
}
