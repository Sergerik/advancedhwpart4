import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);

        System.out.println(list.stream()
                .reduce(1, (Integer x, Integer y) -> x * y));
    }

}
