import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("asd");
        list.add("asdga");
        list.add("d");
        list.add("d");
        list.add("d");
        list.add("d");
        list.add("sd");

        System.out.println(list
                .stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", ")));

    }

}
